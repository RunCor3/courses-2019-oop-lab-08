package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import it.unibo.oop.lab.mvcio.Controller;
import it.unibo.oop.lab.mvcio.SimpleGUI;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    private final Controller cnt;
    private static final String TITLE = "mvcio2";
    private final JFrame frame = new JFrame(TITLE);
    
    public SimpleGUIWithFileChooser() {
        cnt = new Controller();
        JPanel canvas = new JPanel(new BorderLayout());
        JPanel upperPanel = new JPanel(new BorderLayout());
        
        JTextField browseField = new JTextField();
        JButton browseButton = new JButton("Browse");
        JTextArea textArea = new JTextArea();
        JButton button = new JButton("Save");
        
       // textArea.setPreferredSize();
        upperPanel.setBorder(new TitledBorder("Browse Panel"));
        canvas.add(upperPanel, BorderLayout.NORTH);
        upperPanel.add(browseField, BorderLayout.CENTER);
        upperPanel.add(browseButton, BorderLayout.LINE_END);
        
        canvas.add(textArea);
        canvas.add(button, BorderLayout.SOUTH);
        
        browseField.setEditable(false);
        browseField.setText(new Controller().getCurrentFilePath());
        
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                    try {
                        cnt.writeFile(textArea.getText());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
            }
            
        });
        
        browseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                if(fc.showSaveDialog(browseButton) == fc.APPROVE_OPTION) {
                    cnt.setFileAsCurrent(fc.getSelectedFile());
                    browseField.setText(cnt.getCurrentFilePath());
                  
                }
                else if(fc.showSaveDialog(browseButton) == fc.CANCEL_OPTION);
                
                else {
                    JOptionPane.showMessageDialog(frame, "An error occured");
                }
            }
        });
        }
    
        public void display() {
            final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            final int sw = (int) screen.getWidth();
            final int sh = (int) screen.getHeight();
            frame.setSize(sw / 2, sh / 2);
            /*
             * Instead of appearing at (0,0), upper left corner of the screen, this
             * flag makes the OS window manager take care of the default positioning
             * on screen. Results may vary, but it is generally the best choice.
             */
            frame.setLocationByPlatform(true);
            frame.setVisible(true);
    }
    
    
    
    public static void main(String args[]) {
        new SimpleGUIWithFileChooser().display();
    }
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

}
