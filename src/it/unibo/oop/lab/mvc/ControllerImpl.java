package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller{
    private List<String> list = new ArrayList<>();
    private int currIndex;
    
    public ControllerImpl() {
        this.currIndex = 0;
    }
    
    @Override
    public void nextStringToPrint(String string) {
        if(!list.contains(string)) {
            throw new IllegalArgumentException();
        }
        this.currIndex = list.indexOf(string);
        
    }

    @Override
    public String getNextString() {
        return list.get(this.currIndex + 1);
    }

    @Override
    public List<String> getStringsHistory() {
        return this.list;
    }

    @Override
    public void printCurrentString(String string) {
        list.add(string);
        System.out.println(string);
    }

}
