* [33mcommit fa43722c515a6f50f1f93a7d99229bb905b94d2d[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:47:43 2019 +0100
[31m|[m 
[31m|[m     End file with newline
[31m|[m 
* [33mcommit 1640e622cc642b4f539a1083f98889bdb0fbd53b[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:47:32 2019 +0100
[31m|[m 
[31m|[m     Remove useless comment
[31m|[m 
* [33mcommit e9e0170c797e0c17cfcc1dde1152b0c0fe4e6627[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:47:08 2019 +0100
[31m|[m 
[31m|[m     Update the checkers configuration
[31m|[m 
* [33mcommit e2e5b8193a872e06012ce35486bb1c3c52029834[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:46:56 2019 +0100
[31m|[m 
[31m|[m     Fix the project name
[31m|[m 
* [33mcommit 391236e6119e60fe2a403f583781b2434fa70f9c[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:46:32 2019 +0100
[31m|[m 
[31m|[m     Use @Override
[31m|[m 
* [33mcommit e72119182f3e0aa760e39a9861c4a5d2268b13ce[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Wed Nov 6 12:46:19 2019 +0100
[31m|[m 
[31m|[m     Improve README.md
[31m|[m 
* [33mcommit f7954c5ef475b3a512d04ba2649bb573bd1d1a76[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Fri Nov 9 15:41:56 2018 +0100
[31m|[m 
[31m|[m     Bring in exercises from main repo
[31m|[m 
* [33mcommit 07c05f1dbd53aa6bd8d71867293049871ecb5032[m
[31m|[m Author: Danilo Pianini <danilo.pianini@unibo.it>
[31m|[m Date:   Fri Nov 9 15:41:34 2018 +0100
[31m|[m 
[31m|[m     Setup Eclipse
[31m|[m 
* [33mcommit 0625cf27cbcafe697b3dbe52ea7ab71ae10502ac[m
  Author: Danilo Pianini <danilo.pianini@unibo.it>
  Date:   Fri Nov 9 15:33:57 2018 +0100
  
      Create ignore file
